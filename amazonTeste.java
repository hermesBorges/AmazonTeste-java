import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class amazonTeste {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/Users/hiden/Downloads/quality_Assurance/projeto_amazon/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(3000);
        extracted();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos",driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);
    }

    private void extracted() {
        driver.findElement(By.id("nav-hamburger-menu")).click();
    }

    @Test
    public void testarBuscador() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        Thread.sleep(3000);
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-link-accountList")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("ap_email")).sendKeys("aoaoaao@gmail.com");
        Thread.sleep(3000);
        driver.findElement(By.id("continue")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Houve um problema", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/h4")).getText());
        Thread.sleep(3000);
    }
    @Test
    public void testarAbaLivro() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[6]/div/div/div/div/div[2]/div[4]/div/div/a/img")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Exclusivos Amazon", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div/div/div/div/div[1]/div/div/div/h1")).getText());
        Thread.sleep(3000);
    }
    @Test
    public void testarNovidadeAmazon() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);
    }
}

